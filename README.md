The **Okapi OmegaT** project provides plugins for the **open-source translation tool [OmegaT](http://www.omegat.org/ OmegaT).

#### More Information:

For more information about the Okapi Filters Plugin for OmegaT, see the [corresponding page on the main wiki](http://okapiframework.org/wiki/index.php?title=Okapi_Filters_Plugin_for_OmegaT).

Bug report and enhancement requests: https://bitbucket.org/okapiframework/omegat-plugin/issues

#### Downloads:

 * The latest stable version of the Okapi Filters Plugin for OmegaT is at https://bintray.com/okapi/Distribution/OmegaT_Plugin
 * The latest development snapshot is at http://okapiframework.org/snapshots