/*===========================================================================
  Copyright (C) 2013-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.lib.omegat;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import net.sf.okapi.common.Util;
import net.sf.okapi.common.filters.FilterConfigurationMapper;

public class XLIFFOptions extends JDialog {
	
	private static final long serialVersionUID = 1L;
	
	private final Map<String, String> options;
	final JRadioButton rdDefault;
	final JRadioButton rdCustom;
	final JTextField edPath;
	final JButton btGetPath;
	final JCheckBox chkIncludeTransUnitName;
	final JCheckBox chkProtectFinal;
	final JCheckBox chkProtectNoTrans;

	// Internal class for the option actions
	class OptionsListener implements ActionListener {
		@Override
		public void actionPerformed (ActionEvent event) {
			edPath.setEnabled(rdCustom.isSelected());
			btGetPath.setEnabled(rdCustom.isSelected());
		}
	}
	
	/**
	 * Creates an XLIFFOptions object.
	 * @param parent the parent dialog.
	 * @param paramOptions the options.
	 */
	public XLIFFOptions (final Window parent,
		final Map<String, String> paramOptions,
		final String defaultConfigId)
	{
		super(parent, "Okapi XLIFF Filter Options");
		setModal(true);
		
		setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

		if ( paramOptions == null ) {
			this.options = new HashMap<String, String>();
		}
		else {
			this.options = paramOptions;
		}

		Container cp = getContentPane();
	    cp.setLayout(new GridBagLayout());

	    OptionsListener optListner = new OptionsListener();
	    
		rdDefault = new JRadioButton(String.format("Use the default filter settings (%s)", defaultConfigId));
		rdDefault.addActionListener(optListner);
        GridBagConstraints c = new GridBagConstraints();
        c.anchor = GridBagConstraints.FIRST_LINE_START;
        c.gridx = 0; c.gridwidth = 3;
        c.gridy = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(10, 10, 0, 10);
    	cp.add(rdDefault, c);

		rdCustom = new JRadioButton("Use the follwing filter parameters file:");
		rdCustom.addActionListener(optListner);
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0; c.gridwidth = 3;
        c.gridy = 1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(0, 10, 0, 10);
    	cp.add(rdCustom, c);
		
    	ButtonGroup grpOptions = new ButtonGroup();
    	grpOptions.add(rdDefault);
    	grpOptions.add(rdCustom);
		String path = options.get(AbstractOkapiFilter.USE_CUSTOM);
    	String def = options.get(AbstractOkapiFilter.USE_DEFAULT);
    	if (( def == null ) || !def.equals(AbstractOkapiFilter.VALUE_YES) ) {
    		if ( Util.isEmpty(path) ) rdDefault.setSelected(true);
    		else rdCustom.setSelected(true);
    	}
    	else {
    		rdDefault.setSelected(true);
    	}
    	
    	final JPanel pnlPath = new JPanel(new GridBagLayout());
    	
    	edPath = new JTextField();
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0;
        c.gridy = 0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 1.0;
        //c.insets = new Insets(0, 10, 5, 0);
    	pnlPath.add(edPath, c);
		if ( !Util.isEmpty(path) ) edPath.setText(path);
    	
	    btGetPath = new JButton("...");
	    btGetPath.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		JFileChooser dlg = new JFileChooser();
	    		dlg.setDialogTitle("Select a Filter Parameters File");
	    		dlg.setFileFilter(new FileNameExtensionFilter(
	    			"Filter Parameters Files (*"+FilterConfigurationMapper.CONFIGFILE_EXT+")",
	    			FilterConfigurationMapper.CONFIGFILE_EXT.substring(1)));
	    		String path = edPath.getText();
	    		if ( !Util.isEmpty(path) ) {
	    			dlg.setSelectedFile(new File(path));
	    		}
	    		if ( dlg.showOpenDialog(null) == JFileChooser.APPROVE_OPTION ) {
	    			edPath.setText(dlg.getSelectedFile().getAbsolutePath());
	    		}
	    	}
	    });
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_END;
        c.gridx = 1;
        c.gridy = 0;
        c.insets = new Insets(0, 5, 0, 0);
	    pnlPath.add(btGetPath, c);
	    
	    c = new GridBagConstraints();
	    c.anchor = GridBagConstraints.LINE_START;
	    c.gridx = 0; c.gridwidth = 3;
	    c.gridy = 2;
	    c.fill = GridBagConstraints.HORIZONTAL;
	    c.weightx = 1.0;
	    c.insets = new Insets(0, 10, 5, 10);
	    cp.add(pnlPath, c);

    	final JPanel pnlOT = new JPanel(new GridBagLayout());

    	chkIncludeTransUnitName = new JCheckBox("Include the name of the translation unit in comments");
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0; c.gridwidth = 1;
        c.gridy = 0;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(10, 10, 0, 10);
    	pnlOT.add(chkIncludeTransUnitName, c);
    	
    	chkProtectNoTrans = new JCheckBox("Show and protect entries with translate='no' (if not set: they are not extracted)");
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0; c.gridwidth = 1;
        c.gridy = 1;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(0, 10, 0, 10);
    	pnlOT.add(chkProtectNoTrans, c);
    	
    	chkProtectFinal = new JCheckBox("Protect entries with state='final' (if not set: they can be edited)");
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.LINE_START;
        c.gridx = 0; c.gridwidth = 1;
        c.gridy = 2;
        c.weightx = 1.0;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(0, 10, 0, 10);
    	pnlOT.add(chkProtectFinal, c);
    	
	    c = new GridBagConstraints();
	    c.anchor = GridBagConstraints.LINE_START;
	    c.gridx = 0; c.gridwidth = 3;
	    c.gridy = 3;
	    c.fill = GridBagConstraints.HORIZONTAL;
	    c.weightx = 1.0;
	    c.insets = new Insets(0, 0, 20, 0);
        cp.add(pnlOT, c);

        String val = options.get(XLIFFFilter.PROTECT_FINAL);
        chkProtectFinal.setSelected(val==null || val.equals(XLIFFFilter.VALUE_YES));
        val = options.get(XLIFFFilter.INCLUDE_TU_NAME);
        chkIncludeTransUnitName.setSelected(val==null || val.equals(XLIFFFilter.VALUE_YES));
        val = options.get(XLIFFFilter.PROTECT_NOTRANS); 
        chkProtectNoTrans.setSelected(XLIFFFilter.VALUE_YES.equals(val));
        
    	// Do the actions for the options
    	optListner.actionPerformed(null);

    	final JPanel pnlAction = new JPanel(new GridLayout(1, 2, 5, 0));
	    
	    final JButton btOK = new JButton("OK");
	    btOK.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		// Set the default to yes if it's selected
	    		if ( rdDefault.isSelected() ) {
	    			paramOptions.put(AbstractOkapiFilter.USE_DEFAULT, AbstractOkapiFilter.VALUE_YES);
	    		}
	    		// Then look at the path 
	    		String path = edPath.getText();
	    		if ( rdCustom.isSelected() ) {
	    			// It must not be empty to use that option
	    			if ( Util.isEmpty(path) ) {
	    				// Must not be empty
	    				JOptionPane.showMessageDialog(parent, "You must specify a path.", "Missing Path", JOptionPane.ERROR_MESSAGE);
	    				edPath.requestFocusInWindow();
	    				return;
	    			}
	    			// Use custom path
    				paramOptions.put(AbstractOkapiFilter.USE_DEFAULT, AbstractOkapiFilter.VALUE_NO);
	    		}
	    		// Save the path (empty or not in all options)
    			paramOptions.put(AbstractOkapiFilter.USE_CUSTOM, path);
    			
    			// OmegaT behavior
   				paramOptions.put(AbstractOkapiFilter.INCLUDE_TU_NAME,
					chkIncludeTransUnitName.isSelected() ? AbstractOkapiFilter.VALUE_YES : AbstractOkapiFilter.VALUE_NO);
   				paramOptions.put(AbstractOkapiFilter.PROTECT_NOTRANS,
   					chkProtectNoTrans.isSelected() ? AbstractOkapiFilter.VALUE_YES : AbstractOkapiFilter.VALUE_NO);
   				paramOptions.put(AbstractOkapiFilter.PROTECT_FINAL,
   					chkProtectFinal.isSelected() ? AbstractOkapiFilter.VALUE_YES : AbstractOkapiFilter.VALUE_NO);

   				dispose();
	    	}
	    });
	    pnlAction.add(btOK);

    	final JButton btCancel = new JButton("Cancel");
	    btCancel.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		dispose();
	    	}
	    });
	    pnlAction.add(btCancel);
	    
	    c = new GridBagConstraints();
	    c.anchor = GridBagConstraints.LINE_END;
	    c.gridx = 0; c.gridwidth = 3;
	    c.gridy = 4;
	    c.insets = new Insets(0, 10, 10, 10);
	    cp.add(pnlAction, c);

	    pack();
	    setMinimumSize(new Dimension(600, getSize().height));
	    setMaximumSize(new Dimension(600, getSize().height));

        // Center the dialog
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((dim.width-getSize().width)/2, (dim.height-getSize().height)/2);

        // Set the focus properly
    	if ( rdDefault.isSelected() ) rdDefault.requestFocusInWindow();
    	else rdCustom.requestFocusInWindow();
	}
	
	/**
	 * Gets the options.
	 * @return a map of option key-value pairs, never null.
	 */
	public Map<String, String> getOptions () {
		return options;
	}

}
